const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MovieSchema = Schema({
   name: String,
   release_date: String,
   leguage: String,
   photo: String,
   category:{type: String, enum: ['horror','mystery','action','adventure','romance',
                        'sci-fi','comedy','crime','thriller','war','drama','fantasy']},
   description: String,
   IDmovie: {type: Number, unique: true}
});

module.exports = mongoose.model('Movie', MovieSchema);
