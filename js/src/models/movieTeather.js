const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MovieTeatherSchema = Schema({
   movieId:{type: Number},
   teatherId:{type: Number},
   price: {type: Number, default: 0},
   time: String
});

module.exports = mongoose.model('MovieTeather', MovieTeatherSchema);
