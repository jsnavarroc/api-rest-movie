const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeatherSchema = Schema({
   name: String,
   location: String,
   photo: String,
   IDteather: {type: Number, unique: true}
});

module.exports =  mongoose.model('Teather', TeatherSchema);
