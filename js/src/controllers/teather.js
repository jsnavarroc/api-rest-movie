const Teather = require('../models/teather');

// you see all the Teather
function getTeathers (req, res) {
  Teather.find({},(err,teathers)=>{
    if(err) return res.status(500).send({message: `Error making a request: ${err} `});
    if(!teathers) req.status(404).send({message: `These teathers doesn't exist...`});
    res.send(200,{teathers});

  })
}
  // you see a Teather.
function getTeather (req, res) {
    let teatherId = req.params.teatherId;

    Teather.findById(teatherId, (err, teather) => {
      if(err) return res.status(500).send({message: `Error making a request: ${err} `});
      if(!teather) req.status(404).send({message:`The teather doesn't exist...`});

      // res.status(200).send({teather: teather});
      res.status(200).send({teather});
    })
}


// create new Teather.
function saveTeather (req, res) {
    console.log('POST /api/teather');
    console.log(req.body);


    let teather = new Teather();
    teather.name = req.body.name;
    teather.location = req.body.location;
    teather.photo = req.body.photo;
    teather.IDteather = req.body.IDteather;

    teather.save((err,teatherSave)=>{
      if(err) res.status(500).send({message: `Error to save DB: ${err}`})
      res.status(200).send({teather: teatherSave});
    });
}

//update Teather.
function updateteather (req, res) {
    let teatherId = req.params.teatherId;
    let update = req.body;
    Teather.findByIdAndUpdate(teatherId,update ,(err, teatherUpdated) => {
      if(err) res.status(500).send({message: `Error to update teather: ${err}`})
      res.status(200).send({teather: teatherUpdated});

    });
}

//delete Teather.
function deleteTeather (req, res) {

  let teatherId = req.params.teatherId;

          Teather.findById(teatherId, (err, teather) => {
            if(err) return res.status(500).send({message: `Error teather delete: ${err} `});

                  teather.remove(err =>{
                    if(err) res.status(500).send({message: `Error to remove teather: ${err}`})
                    res.status(200).send({teather: `Was deleted correctly`});
                  });

          })
}



module.exports = {
  getTeather,
  getTeathers,
  saveTeather,
  updateteather,
  deleteTeather
}
