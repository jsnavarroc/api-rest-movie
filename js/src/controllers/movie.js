const Movie = require('../models/movie');

  // you see all the movies
function getMovie (req, res) {
    let movieId = req.params.movieId;

    Movie.findById(movieId, (err, movie) => {
      if(err) return res.status(500).send({message: `Error making a request: ${err} `});
      if(!movie) req.status(404).send({message:`The movie doesn't exist...`});

      // res.status(200).send({movie: movie});
      res.status(200).send({movie});
    })
}
// you see a movie.
function getMovies (req, res) {
  Movie.find({},(err,movies)=>{
    if(err) return res.status(500).send({message: `Error making a request: ${err} `});
    if(!movies) req.status(404).send({message:`These movies doesn't exist...`});
    res.send(200,{movies});

  })
}
// create new movie
function saveMovie (req, res) {
    console.log('POST /movies/movie');
    console.log(req.body);


    let movie = new Movie();
    movie.name = req.body.name;
    movie.release_date = req.body.release_date;
    movie.leguage = req.body.leguage;
    movie.photo = req.body.photo;
    movie.category = req.body.category;
    movie.description = req.body.description;
    movie.IDmovie = req.body.IDmovie;

    movie.save((err,movieSave)=>{
      if(err) res.status(500).send({message: `Error to save DB: ${err}`})
      res.status(200).send({movie: movieSave});
    });
}
//update movie
function updateMovie (req, res) {
    let movieId = req.params.movieId;
    let update = req.body;
    Movie.findByIdAndUpdate(movieId,update ,(err, movieUpdated) => {
      if(err) res.status(500).send({message: `Error to update movie: ${err}`})
      res.status(200).send({movie: movieUpdated});

    });
}
//delete movie
function deleteMovie (req, res) {

  let movieId = req.params.movieId;

          Movie.findById(movieId, (err, movie) => {
            if(err) return res.status(500).send({message: `Error movie delete: ${err} `});

                  movie.remove(err =>{
                    if(err) res.status(500).send({message: `Error to remove movie: ${err}`})
                    res.status(200).send({movie: `Was deleted correctly`});
                  });

          })
}

module.exports = {
  getMovie,
  getMovies,
  saveMovie,
  updateMovie,
  deleteMovie
}
