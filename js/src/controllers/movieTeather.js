const MovieTeather = require('../models/movieTeather');

function getMovieTeather (req, res) {
      let movieTeatherId = req.params.movieTeatherId;

    MovieTeather.findById(movieTeatherId, (err, movieTeather) => {
      if(err) return res.status(500).send({message: `Error making a request: ${err} `});
      if(!movieTeather) req.status(404).send({message:`The movieTeather doesn't exist...`});
      res.status(200).send({movieTeather});
    })
}


function getMovieTeathers(req, res) {

  MovieTeather.find({},(err, movieTeather)=>{
    if(err) return res.status(500).send({message: `Error making a request: ${err} `});
    if(!movieTeather) req.status(404).send({message:`These doesn't exist...`});
    res.send(200,{movieTeather});

  })
}



function saveMovieTeather (req, res) {
    console.log('POST /mat/movieTeather');
    console.log(req.body);

    let movieTeather = new MovieTeather();
    movieTeather.movieId = req.body.movieId;
    movieTeather.teatherId = req.body.teatherId;
    movieTeather.price = req.body.price;
    movieTeather.time = req.body.time;

    movieTeather.save((err,movieSave)=>{
      if(err) res.status(500).send({message: `Error to save DB: ${err}`})
      res.status(200).send({movieTeather : movieSave});
    });
}



function deleteMovieTeather (req, res) {

  let movieTeatherId = req.params.movieTeatherId;

          MovieTeather.findById(movieTeatherId, (err, movieTeather) => {
            if(err) return res.status(500).send({message: `Error movieTeather delete: ${err} `});

                  movieTeather.remove(err =>{
                    if(err) res.status(500).send({message: `Error to remove movieTeather: ${err}`})
                    res.status(200).send({movieTeather: `Was deleted correctly`});
                  });

          })
}

module.exports = {
  getMovieTeathers,
  saveMovieTeather,
  getMovieTeather,
  deleteMovieTeather
}
