const express =  require ('express');
const userCtrl = require('../controllers/user')
const auth = require('../middlewares/auth')
const api = express.Router()

const MovieCtrl = require('../controllers/movie');
const TeatherCtrl = require('../controllers/teather');
const MovieTeatherCtrl = require('../controllers/movieTeather');

// you see all the movies
api.get('/movies', MovieCtrl.getMovies);
api.get('/movies/:movieId', MovieCtrl.getMovie);
api.post('/movie', MovieCtrl.saveMovie);
api.put('/movie/:movieId', MovieCtrl.updateMovie);
api.delete('/movie/:movieId', MovieCtrl.deleteMovie);


// HTTP TEATHER
api.get('/teathers', TeatherCtrl.getTeathers);
api.get('/teathers/:teatherId', TeatherCtrl.getTeather);
api.post('/teather', TeatherCtrl.saveTeather);
api.put('/teather/:teatherId', TeatherCtrl.updateteather);
api.delete('/teather/:teatherId', TeatherCtrl.deleteTeather);


// HTTP MovieTeather (JOIN)
api.get('/movieTeather', MovieTeatherCtrl.getMovieTeathers);
api.get('/mat/:movieTeatherId', MovieTeatherCtrl.getMovieTeather);
api.post('/movieTeather', MovieTeatherCtrl.saveMovieTeather);
api.delete('/movieTeather/:movieTeatherId', MovieTeatherCtrl.deleteMovieTeather);


//HTTP user
api.post('/signup', userCtrl.signUp)
api.post('/signin', userCtrl.signIn)
api.get('/private', auth, (req, res) => {
  res.status(200).send({ message: 'You welcome' })
})



module.exports = api;
