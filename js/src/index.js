const mongoose = require('mongoose');
const app = require('./app');
const config = require('./config')

//Connection form BD Mongo
mongoose.connect(config.db,(err, res)=>{
  if(err){
    return console.log(`¡¡Error connection a BD!!`);
  }
  console.log(`Established connection...`);

  app.listen(3001, () => {
    console.log(`API RES run http://localhost:${config.port}`);
  });
})
