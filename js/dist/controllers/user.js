'use strict';

var User = require('../models/user');
var service = require('../services');

function signUp(req, res) {
  var user = new User({
    email: req.body.email,
    displayName: req.body.displayName,
    password: req.body.password
  });

  user.save(function (err) {
    if (err) return res.status(500).send({ message: 'Error create user: ' + err });

    return res.status(201).send({ token: service.createToken(user) });
  });
}

function signIn(req, res) {
  User.find({ email: req.body.email }, function (err, user) {
    if (err) return res.status(500).send({ message: err });
    if (!user) return res.status(404).send({ message: 'Doesn\'t is posible create user' });

    req.user = user;
    res.status(200).send({
      message: 'welcome',
      token: service.createToken(user)
    });
  });
}

module.exports = {
  signUp: signUp,
  signIn: signIn
};