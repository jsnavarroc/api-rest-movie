'use strict';

var Movie = require('../models/movie');

// you see all the movies
function getMovie(req, res) {
  var movieId = req.params.movieId;

  Movie.findById(movieId, function (err, movie) {
    if (err) return res.status(500).send({ message: 'Error making a request: ' + err + ' ' });
    if (!movie) req.status(404).send({ message: 'The movie doesn\'t exist...' });

    // res.status(200).send({movie: movie});
    res.status(200).send({ movie: movie });
  });
}
// you see a movie.
function getMovies(req, res) {
  Movie.find({}, function (err, movies) {
    if (err) return res.status(500).send({ message: 'Error making a request: ' + err + ' ' });
    if (!movies) req.status(404).send({ message: 'These movies doesn\'t exist...' });
    res.send(200, { movies: movies });
  });
}
// create new movie
function saveMovie(req, res) {
  console.log('POST /movies/movie');
  console.log(req.body);

  var movie = new Movie();
  movie.name = req.body.name;
  movie.release_date = req.body.release_date;
  movie.leguage = req.body.leguage;
  movie.photo = req.body.photo;
  movie.category = req.body.category;
  movie.description = req.body.description;
  movie.IDmovie = req.body.IDmovie;

  movie.save(function (err, movieSave) {
    if (err) res.status(500).send({ message: 'Error to save DB: ' + err });
    res.status(200).send({ movie: movieSave });
  });
}
//update movie
function updateMovie(req, res) {
  var movieId = req.params.movieId;
  var update = req.body;
  Movie.findByIdAndUpdate(movieId, update, function (err, movieUpdated) {
    if (err) res.status(500).send({ message: 'Error to update movie: ' + err });
    res.status(200).send({ movie: movieUpdated });
  });
}
//delete movie
function deleteMovie(req, res) {

  var movieId = req.params.movieId;

  Movie.findById(movieId, function (err, movie) {
    if (err) return res.status(500).send({ message: 'Error movie delete: ' + err + ' ' });

    movie.remove(function (err) {
      if (err) res.status(500).send({ message: 'Error to remove movie: ' + err });
      res.status(200).send({ movie: 'Was deleted correctly' });
    });
  });
}

module.exports = {
  getMovie: getMovie,
  getMovies: getMovies,
  saveMovie: saveMovie,
  updateMovie: updateMovie,
  deleteMovie: deleteMovie
};