'use strict';

var Teather = require('../models/teather');

// you see all the Teather
function getTeathers(req, res) {
  Teather.find({}, function (err, teathers) {
    if (err) return res.status(500).send({ message: 'Error making a request: ' + err + ' ' });
    if (!teathers) req.status(404).send({ message: 'These teathers doesn\'t exist...' });
    res.send(200, { teathers: teathers });
  });
}
// you see a Teather.
function getTeather(req, res) {
  var teatherId = req.params.teatherId;

  Teather.findById(teatherId, function (err, teather) {
    if (err) return res.status(500).send({ message: 'Error making a request: ' + err + ' ' });
    if (!teather) req.status(404).send({ message: 'The teather doesn\'t exist...' });

    // res.status(200).send({teather: teather});
    res.status(200).send({ teather: teather });
  });
}

// create new Teather.
function saveTeather(req, res) {
  console.log('POST /api/teather');
  console.log(req.body);

  var teather = new Teather();
  teather.name = req.body.name;
  teather.location = req.body.location;
  teather.photo = req.body.photo;
  teather.IDteather = req.body.IDteather;

  teather.save(function (err, teatherSave) {
    if (err) res.status(500).send({ message: 'Error to save DB: ' + err });
    res.status(200).send({ teather: teatherSave });
  });
}

//update Teather.
function updateteather(req, res) {
  var teatherId = req.params.teatherId;
  var update = req.body;
  Teather.findByIdAndUpdate(teatherId, update, function (err, teatherUpdated) {
    if (err) res.status(500).send({ message: 'Error to update teather: ' + err });
    res.status(200).send({ teather: teatherUpdated });
  });
}

//delete Teather.
function deleteTeather(req, res) {

  var teatherId = req.params.teatherId;

  Teather.findById(teatherId, function (err, teather) {
    if (err) return res.status(500).send({ message: 'Error teather delete: ' + err + ' ' });

    teather.remove(function (err) {
      if (err) res.status(500).send({ message: 'Error to remove teather: ' + err });
      res.status(200).send({ teather: 'Was deleted correctly' });
    });
  });
}

module.exports = {
  getTeather: getTeather,
  getTeathers: getTeathers,
  saveTeather: saveTeather,
  updateteather: updateteather,
  deleteTeather: deleteTeather
};