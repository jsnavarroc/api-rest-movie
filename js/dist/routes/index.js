'use strict';

var express = require('express');
var userCtrl = require('../controllers/user');
var auth = require('../middlewares/auth');
var api = express.Router();

var MovieCtrl = require('../controllers/movie');
var TeatherCtrl = require('../controllers/teather');
var MovieTeatherCtrl = require('../controllers/movieTeather');

// you see all the movies
api.get('/movies', MovieCtrl.getMovies);
api.get('/movies/:movieId', MovieCtrl.getMovie);
api.post('/movie', MovieCtrl.saveMovie);
api.put('/movie/:movieId', MovieCtrl.updateMovie);
api.delete('/movie/:movieId', MovieCtrl.deleteMovie);

// HTTP TEATHER
api.get('/teathers', TeatherCtrl.getTeathers);
api.get('/teathers/:teatherId', TeatherCtrl.getTeather);
api.post('/teather', TeatherCtrl.saveTeather);
api.put('/teather/:teatherId', TeatherCtrl.updateteather);
api.delete('/teather/:teatherId', TeatherCtrl.deleteTeather);

// HTTP MovieTeather (JOIN)
api.get('/movieTeather', MovieTeatherCtrl.getMovieTeathers);
api.get('/mat/:movieTeatherId', MovieTeatherCtrl.getMovieTeather);
api.post('/movieTeather', MovieTeatherCtrl.saveMovieTeather);
api.delete('/movieTeather/:movieTeatherId', MovieTeatherCtrl.deleteMovieTeather);

//HTTP user
api.post('/signup', userCtrl.signUp);
api.post('/signin', userCtrl.signIn);
api.get('/private', auth, function (req, res) {
  res.status(200).send({ message: 'You welcome' });
});

module.exports = api;