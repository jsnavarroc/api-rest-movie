'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var hbs = require('express-handlebars');
var app = express();
var api = require('./routes');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.engine('.hbs', hbs({
  defaultLayout: 'default',
  extname: '.hbs'
}));

app.set('view engine', '.hbs');

app.use('/api', api);

app.get('/movie', function (req, res) {
  res.render('movie');
});
app.get('/teather', function (req, res) {
  res.render('teather');
});

module.exports = app;