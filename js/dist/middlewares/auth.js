'use strict';

var services = require('../services');

function isAuth(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'Don\'t can\'t ton inside' });
  }

  var token = req.headers.authorization.split(' ')[1];

  services.decodeToken(token).then(function (response) {
    req.user = response;
    next();
  }).catch(function (response) {
    res.status(response.status);
  });
}

module.exports = isAuth;