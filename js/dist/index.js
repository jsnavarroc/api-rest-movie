'use strict';

var mongoose = require('mongoose');
var app = require('./app');
var config = require('./config');

//Connection form BD Mongo
mongoose.connect(config.db, function (err, res) {
  if (err) {
    return console.log('\xA1\xA1Error connection a BD!!');
  }
  console.log('Established connection...');

  app.listen(3001, function () {
    console.log('API RES run http://localhost:' + config.port);
  });
});