'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MovieTeatherSchema = Schema({
   movieId: { type: Number },
   teatherId: { type: Number },
   price: { type: Number, default: 0 },
   time: String
});

module.exports = mongoose.model('MovieTeather', MovieTeatherSchema);