'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TeatherSchema = Schema({
   name: String,
   location: String,
   photo: String,
   IDteather: { type: Number, unique: true }
});

module.exports = mongoose.model('Teather', TeatherSchema);